---
title: Home
---

[<img src="portrait.jpg" style="max-width:25%;min-width:40px;float:left;" alt="Github repo" hspace="10" />](https://github.com/yihui/hugo-xmin)

# Spencer Woody

## PhD Student, Statistics

Hi, I’m Spencer Woody. I am a second year PhD student in statistics at the University of Texas at Austin, currently working with Professor James Scott. My current research interests are in spatial smoothing and post-selection inference in multiple testing scenarios.

I also serve as a research assistant in the lab of Dr. Livia Eberlin in the Department of Chemistry, where we use metabolomic mass spectrometry data for cancer detection in applications such as surgical margin assessment and fine needle aspirations of the thyroid.

You can see my [posts](/post/), projects, and CV.

### Research Interests

- Bayesian statistics
- Post-selection inference in multiple testing
- Spatial statistics
- High-throughput bioinformatics
- Medical research methodology

### Education

- Duke University (2015)
  - BS in Economics and Statistical Science

### Contact

`spencer.woody@utexas.edu`

GDC 7.418D  
2317 Speedway D9800  
Austin, TX 78712  
USA

### Posts
